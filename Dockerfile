FROM python:3.11-slim

ARG ENVIRONMENT

ENV ENVIRONMENT=${ENVIRONMENT}

WORKDIR /app

RUN apt-get update && \
    apt-get install -y --no-install-recommends curl apt-utils && \
    rm -rf /var/lib/apt/lists/*

RUN pip install -U poetry pip setuptools && poetry config virtualenvs.create false

COPY poetry.lock pyproject.toml /app/

RUN poetry install --no-root

COPY . .

EXPOSE 8000